#!/bin/bash
#!/bin/zsh
# ^- Must work in both

# ===============================================================
# shell_shared_desktop
# Eskild Hustvedt
# ===============================================================
# This file contains shared aliases and functions that work both under
# zsh and bash that should only be available on Fedora Silverblue, which
# is kind of a special little snowflake.

if [ "x$TOOLBOX_PATH" = "x" ]; then
    if [ "$_RUN_ON_HOST" != "1" ]; then
        echo "This terminal is not inside toolbox"
    fi
    # When not inside a toolbox, wrap some commands for easy access
    for cmd in htop make feh mpv tig rg screen; do
        alias $cmd="_tbRun $cmd"
    done
    # Wrap some commands that is not in the path used for 'toolbox run'
    for cmd in gpgpwd; do
        local _cmd="$(which "$cmd" 2>/dev/null)"
        if [ "x$_cmd" != "x" ]; then
            alias $cmd="_tbRun \"$_cmd\""
        fi
    done
    alias convert="_tbRun gm convert"
    alias tmux="_tbRun tmux -u"
    alias mosh="_tbRun mosh -p 60000:60100"
    # And finally, vim
    if ! which --skip-alias vim &>/dev/null && ! which --skip-alias nvim &>/dev/null; then
        alias REAL_VIM="_tbRun nvim"
    fi

    unalias tb

    function _tbRun ()
    {
        if [ "x$TOOLBOX_DEFAULT_CONTAINER" != "x" ]; then
            TB_CONTAINER="$TOOLBOX_DEFAULT_CONTAINER"
        else
            TB_CONTAINER="fedora-toolbox-$(grep VERSION_ID= /etc/os-release|perl -p -E 's/.+VERSION_ID=//')"
        fi
        if [ "$SSH_CLIENT" != "" ]; then
            touch ~/.shell_shared_toolbox-isUnderSSH
        else
            rm -f ~/.shell_shared_toolbox-isUnderSSH >/dev/null
        fi
        toolbox run --container $TB_CONTAINER "$@"
    }
    function tb ()
    {
        if [ "x$TOOLBOX_PATH" != "x" ]; then
            echo "Already in a toolbox. Use the 'toolbox' command if you really meant it."
            return 1
        fi
        if [ "x$*" != "x" ]; then
            case "$1" in
                --help|enter|list|create|-r)
                    toolbox "$@"
                    return $?
                    ;;
                -e|-x)
                    _tbRun "$SHELL" -l
                    exit $?
                    ;;
                *)
                    _tbRun "$@"
                    return $?
                    ;;
            esac
        else
            _tbRun "$SHELL" -l
            return $?
        fi
    }
    alias _toolbox="tb"
    if [ -e "$HOME/.shell_shared_desktop_silverblue_local" ]; then
        source "$HOME/.shell_shared_desktop_silverblue_local"
    fi
fi
