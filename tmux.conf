# Automatic window titles
set-option -g set-titles on
set-window-option -g automatic-rename on

# Enable focus events
set-option -g focus-events on

# Disable the right part of the status line
set -g status-right ''
set -g status-left ''

# Terminal type configuration
set -sa terminal-overrides ",xterm-256color:Tc"
set -g default-terminal "tmux-256color"

# Use zsh by default
set -g default-shell /bin/zsh

# Allow the shell to rename windows by default
# (used to default to on in tmux 2.6 and older)
set -g allow-rename on

# Don't wait during escape - slows down editing in vim too much
set -sg escape-time 1

# C-a hjkl navigation
unbind k
unbind l
bind j select-pane -D
bind h select-pane -L
bind k select-pane -U
bind l select-pane -R

# C-a sS for splitting
bind S split-window -v -p 30
bind s split-window -v

# | for vertical splits
bind | split-window -h

# Sets up the main session
source-file ~/.tmux-start.conf

# --
# screen-keys.conf from tmux
# --

# Set the prefix to ^A.
unbind C-b
set -g prefix ^A
bind a send-prefix

# Bind ^b as a pageup alternative
bind ^b copy-mode -u

# Bind appropriate commands similar to screen.
# lockscreen ^X x
unbind ^X
bind ^X lock-server

# screen ^C c
unbind ^C
bind ^C new-window
unbind c
bind c new-window

# detach ^D d
unbind ^D
bind ^D detach

# displays *
unbind *
bind * list-clients

# next ^@ ^N sp n
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window

# title A
unbind A
bind A command-prompt "rename-window %%"

# other ^A
unbind ^A
bind ^A last-window

# prev ^H ^P p ^?
unbind ^H
bind ^H previous-window
unbind ^P
bind ^P previous-window
unbind p
bind p previous-window
unbind BSpace
bind BSpace previous-window

# windows ^W w
unbind ^W
bind ^W list-windows
unbind w
bind w list-windows

# quit \
unbind '\'
bind '\' confirm-before "kill-server"

# kill K k
unbind K
bind K confirm-before "kill-window"

# redisplay ^L l
unbind ^L
bind ^L refresh-client

# :kB: focus up
unbind Tab
bind Tab select-pane -t:.+
bind C-Tab select-pane -t:.+
unbind BTab
bind BTab select-pane -t:.-
bind C-BTab select-pane -t:.-

# " windowlist -b
unbind '"'
bind '"' choose-window

# --
# Colorscheme
# Based upon solarized dark with several modifications
# --
#### COLOUR (Solarized dark)

# default statusbar colors
set-option -g status-style fg=yellow,bg=black #yellow and base02

# default window title colors
set-window-option -g window-status-style fg=brightblue,bg=default #base0 and default
#set-window-option -g window-status-style dim

# active window title colors
set-window-option -g window-status-current-style fg=brightred,bg=default #orange and default
#set-window-option -g window-status-current-style bright

# pane border
set-option -g pane-border-style fg=black #base02
set-option -g pane-active-border-style fg=brightgreen #base01

# message text
set-option -g message-style fg=brightred,bg=black #orange and base01

# pane number display
set-option -g display-panes-active-colour blue #blue
set-option -g display-panes-colour brightred #orange

# clock
set-window-option -g clock-mode-colour green #green

# bell
set-window-option -g window-status-bell-style fg=black,bg=red #base02, red
