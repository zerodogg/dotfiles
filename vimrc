" .vimrc for Eskild Hustvedt

" Key bindings set here:
" <Leader> is space
" F10        - Toggle paste mode
" F2         - make
" F9         - toggle scrollbind
" W          - Write (in normal mode)
" ,#         - comment
" ,u         - uncomment
" C-p        - Open up the file searcher with FZF
" C-t        - Open up a new tab with the FZF searcher open
" <Leader>d  - git diff on the current file
" <Leader>b  - Open a buffer explorer
" <Leader>f  - Open a file explorer
" <Leader>s  - Open a search window (for the current buffer)
" <Leader>m  - Open the MRU
" <Leader>o  - Open up the FZF file searcher
" <Leader>y  - copy to system clipboard
" <Leader>p  - paste from system selection clipboard
" <Leader>P  - paste from system clipboard

" Use Vim settings, not vi settings.
" This must be first, because it changes other options as a side effect.
set nocompatible

let mapleader = "\<Space>"

" Bundles"{{{
if isdirectory(glob('~/.vim/vim-plug/'))
call plug#begin('~/.vim/vim-plug/')
    " Essentials {{{
            " solarized colors
    if has('nvim-0.10')
        Plug 'hdonnay/solarized'
    else
        Plug 'altercation/vim-colors-solarized'
    endif
    " }}}
    if !filereadable(expand('~/.vim-plug-minimal'))
        " Additional and improved filetypes {{{
            " Better markdown support
        Plug 'plasticboy/vim-markdown'
            " iCalendar syntax
        Plug 'vim-scripts/icalendar.vim'
            " Enhanced po-editing
        Plug 'vim-scripts/po.vim--Jelenak'
            " LaTeX support
        Plug 'LaTeX-Box-Team/LaTeX-Box'
            " GTK and SQlite hilight
        Plug 'vim-scripts/gtk-vim-syntax'
        Plug 'vim-scripts/sqlite_c'
            " An excellent outline format for vim
        Plug 'vimoutliner/vimoutliner'
            " Table-like CSV view
        Plug  'chrisbra/csv.vim', { 'for':'csv' }
            " dokuwiki syntax
        Plug 'nblock/vim-dokuwiki'
            " Perl (also loads perlomni later)
        Plug 'vim-perl/vim-perl6'
        Plug 'zerodogg/vim-mason'
        Plug 'jbob/vim-perltidy', { 'commit':'8f6a851' }
            " todo.txt
        Plug 'freitass/todo.txt-vim'
        Plug 'elentok/todo.vim'
            " Web (HTML5, JSON, CSS, plus SCSS, tag auto-close,
            "  mustache, eco templates)
        Plug 'alvan/vim-closetag', { 'for':'html' }
        Plug 'AndrewRadev/vim-eco'
        " Raku
        Plug 'Raku/vim-raku'
        " nunjucks syntax hilight
        Plug 'Glench/Vim-Jinja2-Syntax'
        " All other filetypes (except raku)
        let g:polyglot_disabled = ['raku']
        Plug 'sheerun/vim-polyglot'
        "}}}
        " Programming and editing utilities {{{
                " Git utilities
        Plug 'tpope/vim-fugitive', { 'augroup' : 'fugitive'}
        if(v:version < 702)
                Plug 'tpope/vim-git'
        end
                " Clean up whitespace
    "    Plug 'bronson/vim-trailing-whitespace'
                " Validate source
        Plug  'dense-analysis/ale', { 'on_event' : 'InsertEnter', 'on':'ALELint' }

                " Align anything (:Tabularize)
        Plug  'godlygeek/tabular', { 'on': 'Tabularize' }
        " Various
                " support for async actions
        Plug 'shougo/vimproc.vim', {'build': 'make'}
        " Manpage support
        Plug  'jez/vim-superman',{'on': 'SuperMan'}
            " Distraction-free writing
        Plug  'junegunn/goyo.vim', {'on':['Goyo']}
            " :SudoWrite
        Plug  'chrisbra/SudoEdit.vim', {'on':[ 'SudoWrite', 'SudoRead' ] }
            " interface to files, buffers, MRU etc.
            " NOTE: Expects FZF to be installed using the distro package manager
        if executable('fzf')
            Plug 'junegunn/fzf'
            Plug 'junegunn/fzf.vim'
        end
            " Addon that finds the project root for the current project (or tries
            " to)
        Plug 'dbakker/vim-projectroot'
            " Slightly improved netrw (file browser)
        Plug  'tpope/vim-vinegar'
            " Mutliple cursors
        Plug  'terryma/vim-multiple-cursors'
            " Interactive REPL
        Plug 'metakirby5/codi.vim'
            " Auto-update copyright notices
        Plug 'mrdubya/copyright-updater', {'commit':'7eb6413cfe1c98583f33868ff9fa354838276d69'}
        "}}}
        " Completion {{{
        if executable('node')
            Plug 'neoclide/coc.nvim', {'branch': 'release'}
            Plug 'yaegassy/coc-tailwindcss3', {'do': 'yarn install --frozen-lockfile'}
            " See the CoC vim configuration later for additional coc extensions
        endif
        "}}}
    endif
call plug#end()
endif
"}}}

" Local RC file (.head-version) {{{
if filereadable(glob("~/.vimrc_local.head"))
	source ~/.vimrc_local.head
endif "}}}

" CoC vim configuration {{{1
silent! if(executable('node')) && has_key(plugs, 'coc.nvim')
    " Extensions
    let g:coc_global_extensions = ["coc-flow","coc-tsserver","coc-prettier", "@yaegassy/coc-tailwindcss3"]

    " Set up a :Prettier command
    command! -nargs=0 Prettier :CocCommand prettier.formatFile

    " TextEdit might fail if hidden is not set.
    set hidden

    " Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
    " delays and poor user experience.
    set updatetime=300

    " Don't pass messages to |ins-completion-menu|.
    set shortmess+=c

    function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
    endfunction

    " Insert <tab> when previous text is space, refresh completion if not.
    inoremap <silent><expr> <TAB>
                \ coc#pum#visible() ? coc#pum#next(1):
                \ <SID>check_back_space() ? "\<Tab>" :
                \ coc#refresh()
    inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

    " Use <c-space> to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

    " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
    " position. Coc only does snippet and additional edit on confirm.

    " Use `[g` and `]g` to navigate diagnostics
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " GoTo code navigation.
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window.
    nnoremap <silent> K :call <SID>show_documentation()<CR>

    function! s:show_documentation()
      if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
      else
        call CocAction('doHover')
      endif
    endfunction

    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call CocActionAsync('highlight')

    " Symbol renaming.
    nmap <leader>crn <Plug>(coc-rename)

    " Formatting selected code.
    xmap <leader>f  <Plug>(coc-format-selected)
    nmap <leader>f  <Plug>(coc-format-selected)

    augroup mygroup
      autocmd!
      " Setup formatexpr specified filetype(s).
      autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
      " Update signature help on jump placeholder.
      autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
    augroup end

    " Applying codeAction to the selected region.
    " Example: `<leader>aap` for current paragraph
    xmap <leader>a  <Plug>(coc-codeaction-selected)
    nmap <leader>a  <Plug>(coc-codeaction-selected)

    " Remap keys for applying codeAction to the current line.
    nmap <leader>ac  <Plug>(coc-codeaction)
    " Apply AutoFix to problem on the current line.
    nmap <leader>qf  <Plug>(coc-fix-current)

    " Introduce function text object
    " NOTE: Requires 'textDocument.documentSymbol' support from the language server.
    xmap if <Plug>(coc-funcobj-i)
    xmap af <Plug>(coc-funcobj-a)
    omap if <Plug>(coc-funcobj-i)
    omap af <Plug>(coc-funcobj-a)

    " Use <TAB> for selections ranges.
    " NOTE: Requires 'textDocument/selectionRange' support from the language server.
    " coc-tsserver, coc-python are the examples of servers that support it.
    nmap <silent> <TAB> <Plug>(coc-range-select)
    xmap <silent> <TAB> <Plug>(coc-range-select)

    " Add `:Format` command to format current buffer.
    command! -nargs=0 Format :call CocAction('format')

    " Add `:Fold` command to fold current buffer.
    command! -nargs=? Fold :call     CocAction('fold', <f-args>)

    " Add `:OR` command for organize imports of the current buffer.
    command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

    " Mappings using CoCList:
    " Show all diagnostics.
    nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
    " Manage extensions.
    nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
    " Show commands.
    nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
    " Find symbol of current document.
    nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
    " Search workspace symbols.
    nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
    " Do default action for next item.
    nnoremap <silent> <space>j  :<C-u>CocNext<CR>
    " Do default action for previous item.
    nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
    " Resume latest coc list.
    nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
end
" }}}

" Base vim/novim settings"{{{

" Allow backspacing over everything in insert mode
set backspace=indent,eol,start
" Switch on autoindent
set autoindent
filetype plugin indent on
" Enable the matchit plugin (vim 6.x+)
runtime macros/matchit.vim
" Switch syntax highlighting and highlighting of the last used search
" pattern on.
syntax on
set hlsearch
" Enable smart tab
set smarttab
" Enable ignorecase (set ignorecase) except when the search expression
" contains an uppercase character (set smartcase)
set ignorecase
set smartcase
" Enable enhanced command-line completion
set wildmenu
" Wrap text
set wrap
" Fold at syntax level
set foldmethod=syntax
" Don't fold too much by default. This is overridden for some filetypes.
set foldnestmax=2
" Output a pretty title
set title
set titlestring=Vim:\ %t%m%r
" Default everything to utf-8
set fileencoding=utf-8
set encoding=UTF-8
" Keep a backup file
set backup
" Keep 250 lines of command line history
set history=250
" Show the cursor position at all times
set ruler
" Display incomplete commands
set noshowcmd
" Do incremental searching
set incsearch
" Indenting looks nicer with this
set tabstop=4 shiftwidth=4 softtabstop=4
    " Ensure this gets set
autocmd FileType * setlocal tabstop=4 shiftwidth=4 softtabstop=4
" Expand tabs
set expandtab
" Scroll if we're 5 or less lines near the top/bottom of the screen
set scrolloff=5
" A bit more paranoid about saving the swap file
set updatecount=100
" Ignore these files
set wildignore=*.o,*~,*.swp,*.tmp
" Don't scan all included files during autocomplete (ends up being rather
" slow)
set complete-=i
" Disable mouse support in terminals
if !has("gui_running")
    set mouse=
endif
" Close the taglist on selection
silent! let Tlist_Close_On_Select=1
" Solarized colors
set bg=dark
set t_Co=16
silent! colorscheme solarized
" Workaround for CoC popups
highlight CocFloating ctermbg=black ctermfg=white
" Standard hardcopy-font
silent! set printfont=Terminus\ 12
" Vim swapfile locations
" Default to ~/.vim/swap if it exists, otherwise use default vim paths.
set dir=~/.vim/swap,.,~/tmp/,/var/tmp,/tmp
" Only display one block to hilight indention levels
let g:indent_guides_guide_size = 1
" Disable mouse in terminals, I'll use the terminal's handling
if !has("gui_running")
    set mouse=
endif
" Set foldmethod to manual when in insert mode. Avoids slowdowns caused by
" vim constantly trying to open/close and update folds while typing.
autocmd InsertEnter * let w:last_fdm=&foldmethod | setlocal foldmethod=manual
autocmd InsertLeave * let &l:foldmethod=w:last_fdm
" Ignore whitespace errors in certain buffers
let g:extra_whitespace_ignored_filetypes = [ 'vimshell', 'diff', 'gitcommit', 'mail', 'help', '' ]
" The vim wiki path
let g:vimwiki_list = [{'path': '~/Documents/wiki/'} ]
" Show fzf on top
let g:fzf_layout = { 'up': '~40%' }
" Increase the delay before ale checks-as-you-type
let g:ale_lint_delay = 2000
" Make ale lint upon exiting insert mode
let g:ale_lint_on_insert_leave = 1
" Adds a :FixWhitespace command
command! FixWhitespace %s/\s\+$//e
"}}}

" Vim-only settings"{{{
if !has('nvim')
    " Enable xterm-style behaviour
    behave xterm
    " Force-enable bce under screen
    if &term == "screen"
        set term=screen-bce
    elseif &term == "screen-256color"
        set term=screen-256color-bce
    endif
    " Make some terminal changes when under screen
    if &term == "screen" || &term == "screen-bce" || &term == "screen-256color" || &term == "screen-256color-bce" || &term == "tmux-256color"
        " Set the title using screen's functions instead of default which is
        " xterm
        set t_ts=k
        set t_fs=\
        " Make sure we use the alternate screen, this should always be on when
        " *I* am using screen anyway
        set t_ti=[?1049h
        set t_te=[?1049l
        " Fix broken end key on some boxes
        set t_@7=[4~
    endif
endif
"}}}

" Neovim-only settings"{{{
if has('nvim')
    " Neovim doesn't use t_*, so use escape sequences in the titlestring
    " to fix the terminal title
    if $TERM =~ 'screen\|tmux'
        let &titlestring = "\ek" . &titlestring . "\e\\"
    end
end
"}}}

" Keymappings and commands"{{{

" Map f2 to :w, :make, :cw so that we can check syntax with f2
map <F2> :w<CR>:make<CR>:cw<CR>
" Map <leader>d to git diff
map <leader>d :Gdiff<CR>
" Map F9 to scrollbind toggle
map <F9> :set scrollbind!<CR>
" Map ¤ (shift+4 on Norwegian keyboards) to <End> ($ is default, which I think
"  works on English keyboards, but that would require using alt here, which
"  isn't that easily accessible.)
map ¤ <END>
" Comment out the marked line(s) with #
map ,# :s/^/#/<CR><Esc>:nohlsearch<CR>
" Uncomming the marked line(s) that has #
map ,u :s/^#//<CR><Esc>:nohlsearch<CR>
" Grep for TODO and put it into the error win
command TODO :sil cclose | call setqflist([]) | exe 'g/TODO\|FIXME/caddexpr expand("%") . ":" . line(".") . ":" .getline(".")' | cw
" New tab
map <C-t> :tabnew<CR><Leader>R
" Overrides the neomru default ignore pattern to not ignore files in /mnt
let g:neomru#file_mru_ignore_pattern =
      \'\~$\|\.\%(o\|exe\|dll\|bak\|zwc\|pyc\|sw[po]\)$'.
      \'\|\%(^\|/\)\.\%(hg\|git\|bzr\|svn\)\%($\|/\)'.
      \'\|^\%(\\\\\|/media/\|/temp/\|/tmp/\|\%(/private\)\=/var/folders/\)'.
      \'\|\%(^\%(fugitive\)://\)'
" Ignore vim help files in neomru
" Open up the FZF file searcher
"  This uses .ignore instead of .gitignore (to allow for searching suprepos
"  that it would usually ignore).
nnoremap <Leader>o :ProjectRootExe call fzf#run({"source": "rg --files --no-ignore-vcs --hidden --glob '!.git' --glob '!*~' --glob '!*.swp'","sink":"e"})<CR>
" FZF seracher through git tracked files
nnoremap <Leader>g :GFiles<CR>
" MRU
nnoremap <Leader>r :History<cr>
nmap <Leader>m :History<CR>
" MRU or FZF searcher
nnoremap <expr> <Leader>R expand('~') == getcwd() ? ':History<CR>' : ':call feedkeys("<Leader>o")<CR>'
" Toggle a file/dir browser
map <Leader>f <Plug>VinegarUp
" Toggle a buffer explorer
map <Leader>b :Buffers<cr>
" Toggle a search window
nmap <Leader>s :Lines<cr>
" Toggle a ripgrep window
nnoremap <Leader>a :Rg<Cr>
" Use shift-insert as MiddleMouse, ie. to paste (like in xterm)
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>
" Use F10 to toggle paste mode
set pastetoggle=<F10>
" I :w often - this is quicker
nnoremap <leader>w :w<CR>
" Copy to clipboard with <Leader>y, paste with <Leader>p
vmap <Leader>y "+y
nmap <Leader>p "*p
vmap <Leader>p "*p
nmap <Leader>P "+P
vmap <Leader>P "+P
" Start the vim wiki
nmap <Leader>ww <Plug>VimwikiIndex
" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif
" Disable Q-to-enter-Ex-mode
nnoremap Q <nop>
" Use C-k and C-j to jump to prev/next ale error
nmap <silent> <C-k> <Plug>(ale_previous)
nmap <silent> <C-j> <Plug>(ale_next)
"}}}

" Autocommands"{{{

" Jump to the last known position on startup
autocmd BufReadPost *
			\ if line("'\"") > 0 && line("'\"") <= line("$") |
			\   exe "normal! g`\"" |
			\ endif
" Wiki auto commit (based upon https://gist.github.com/rsommer/6614941db7af813772f8)
" requires python2-gitpython
function! CommitFileToGit()
python << EOF
import vim, git
curfile = vim.current.buffer.name
if curfile:
    try:
        repo = git.Repo(curfile)
        repo.git.add(curfile)
        repo.git.commit(m='Update {curfile}'.format(curfile=curfile))
    except (git.InvalidGitRepositoryError, git.GitCommandError):
        pass
EOF
endfunction
autocmd BufWritePost *.wiki call CommitFileToGit()
"}}}

" Filetype-specific settings"{{{1
" -- PO settings --"{{{
let g:po_translator="Eskild Hustvedt <i18n@zerodogg.org>"
let g:po_lang_team="Norwegian Nynorsk <i18n-no@lister.ping.uio.no>"
"}}}
" -- Perl settings --"{{{
" Enable perl folding by default
let perl_fold = 1
" Highlight POD
let perl_include_pod = 1
" Compiler
autocmd FileType perl compiler perl
" Make settings
autocmd FileType perl setlocal makeprg=perl\ -c\ %
"}}}
" -- Raku settings --"{{{
" Enable folding by default
let raku_fold = 1
" rakumod is raku modules
autocmd BufNewFile,BufRead *.rakumod set ft=raku
"}}}
" -- PHP settings --"{{{
" Enable PHP folding by default
let php_folding = 1
" Show SQL syntax in PHP strings
let php_sql_query=1
" Show HTML syntax in PHP strings
let php_htmlInStrings=1
" Show some more errors
let php_parent_error_close=1
let php_parent_error_open=1
" Hilight baselib stuff
let php_baselib=1
" ctp is php
autocmd BufNewFile,BufRead *.ctp set ft=php
" Make program settings
autocmd FileType php setlocal makeprg=php\ -l\ %
autocmd FileType php setlocal errorformat=%m\ in\ %f\ on\ line\ %l
"}}}
" -- GIT settings --"{{{
autocmd FileType gitcommit setlocal foldmethod=manual
"}}}
" -- XML settings --"{{{
autocmd FileType xml setlocal makeprg=xmllint\ --noout\ %
autocmd FileType xml setlocal errorformat=%f\:%l%m
"}}}
" -- Java settings --"{{{
" This enables folding for java. Not very pretty folding, but at least usable.
autocmd FileType java syn region myFold start="{" end="}" transparent fold
"}}}
" -- Nunjucks --"{{{
autocmd BufNewFile,BufRead *.njk set ft=jinja2.html
"}}}
" -- git commit --"{{{
autocmd FileType gitcommit setlocal tw=0
"}}}
" -- LaTeX settings --"{{{
let g:tex_fold_enabled=1
" Set ft
autocmd BufNewFile,BufRead *.tex set ft=tex
" Enable spell checking
autocmd FileType tex setlocal spell
set spelllang=nn,en
" Use default foldnestmax (20)
autocmd FileType tex setlocal foldnestmax=20
autocmd FileType tex setlocal tw=80
"}}}
" --- Markdown settings ---"{{{
" Filetype detection
autocmd! BufRead,BufNewFile *.md setfiletype markdown
" Enable spell checking
autocmd FileType markdown setlocal spell spellcapcheck=off
" Textwidth for markdown
autocmd FileType markdown setlocal tw=79
" Disable folding, it's very broken in many cases
autocmd FileType markdown setlocal nofoldenable
"}}}
" --- Ruby settings ---"{{{
" I really don't care what the plugin devs consider to be the recommended
" style
let g:ruby_recommended_style=0
"}}}
" --- C/C++ settings ---"{{{
autocmd FileType c,cpp setlocal tabstop=2 shiftwidth=2
let c_no_comment_fold=1
let c_no_block_fold=1
let c_space_errors=0
let c_gnu=1
"}}}
" --- mail settings --"{{{
autocmd FileType mail setlocal foldmethod=manual
"}}}
" --- BASH settings ---"{{{
" Folding doesn't work very well, so use a basic one instead
let g:sh_fold_enabled=1
"}}}
" --- iCalendar settings ---"{{{
" Filetype detection
autocmd! BufRead,BufNewFile *.ics setfiletype icalendar
"}}}
" --- HTML ---"{{{
autocmd! FileType html syntax sync minlines=2000 maxlines=4000
" Additional common Mason files
autocmd BufNewFile,BufRead autohandler set ft=mason
autocmd BufNewFile,BufRead dhandler set ft=mason
autocmd BufNewFile,BufRead *.mas set ft=mason
" Load the html ftplugin when using mason
autocmd FileType mason so $VIMRUNTIME/ftplugin/html.vim
"}}}
" --- JavaScript ---"{{{
let g:javascript_plugin_flow = 1
let g:jsx_ext_required = 0
let g:vim_jsx_pretty_highlight_close_tag = 1
autocmd BufNewFile,BufRead *.{js,mjs,jsm,es,es6,jsx} let b:ale_linters = ["eslint","flow"]
autocmd BufNewFile,BufRead *.{js,mjs,jsm,es,es6,jsx} :ALELint
autocmd BufNewFile,BufRead *.{js,mjs,jsm,es,es6,jsx} syntax sync minlines=2000 maxlines=10000
"}}}
" --- Other filetype detection ---"{{{
autocmd BufNewFile,BufRead Makefile.cfg set ft=make
autocmd BufNewFile,BufRead *.run set ft=sh
autocmd BufNewFile,BufRead *.tt++ set ft=tt
autocmd BufNewFile,BufRead *.j2 set ft=jinja
"}}}
" --- Vimoutliner settings ---"{{{
" Use dark background, set the filetype, enable folding
autocmd BufNewFile,BufRead *.otl setlocal ft=votl foldlevel=0 spellcapcheck=off nospell
" Don't make fold text too short
silent! let g:vo_fold_length=79
"}}}
" --- SASS ---"{{{
autocmd! FileType scss syntax sync minlines=1000
"}}}
" --- vimwiki ---"{{{
autocmd! FileType vimwiki setlocal foldlevel=1 concealcursor=n conceallevel=2 tw=79
" Disable conceal in insert mode, and disable concealcursor after having been
" in insert mode
autocmd FileType vimwiki :autocmd InsertEnter <buffer> setlocal conceallevel=0
autocmd FileType vimwiki :autocmd InsertLeave <buffer> setlocal conceallevel=2 concealcursor=""
"}}}
" --- dart ---"{{{
function! DartRemapKeys()
    " Use `[g` and `]g` to navigate diagnostics
    nmap <silent> <C-k> <Plug>(coc-diagnostic-prev)
    nmap <silent> <C-j> <Plug>(coc-diagnostic-next)
endfunction
autocmd FileType dart call DartRemapKeys()
autocmd FileType dart setlocal tabstop=2 shiftwidth=2 softtabstop=2
"}}}
" --- todo.txt ---"{{{
autocmd FileType todo nnoremap <script> <silent> <buffer> <leader>S :sort<CR>
autocmd FileType todo nnoremap <script> <silent> <buffer> <leader>S@ :sort /.\{-}\ze@/ <CR>
autocmd FileType todo nnoremap <script> <silent> <buffer> <leader>S+ :sort /.\{-}\ze+/ <CR>
autocmd FileType todo nnoremap <script> <silent> <buffer> <leader>b :call TodoTxtPrioritizeAdd('B')<CR>
"}}}
"1}}}

" GUI settings {{{1

if has("gui_running")
	" I don't like blinking cursors - and I like my block cursor
	set guicursor+=a:blinkon0-block
	" Console dialogs instead of GUI ones,
	" Remove all scrollbars+toolbar+menubar
	set go+=cg go-=r go-=R go-=l go-=L go-=T go-=e go-=m
	" Hide the mouse when typing
	set mousehide
	" I like Terminus
	set guifont=Terminus\ 12
	" Start gvim in the largest size possible
	set columns=999 lines=999
endif
" }}}

" Typos and other automatic substitutions{{{1

" Typos
abbreviate tempalte template
abbreviate fuction function
abbreviate =< <=
" These won't get applied in most cases, but won't hurt to
" have them.
abbreviate if( if (
abbreviate }else{ }<CR>else<CR>{
abbreviate ){ )<CR>{
abbreviate ÆÆ **
abbreviate '/ÆÆ' '/**'
abbreviate serach search

" This remaps a multi-byte space character into a normal one
imap   <space>

" Per-filetype typos and fixes
fun PerFT_typo()
	if &ft == 'perl'
		abbreviate elseif elsif
		abbreviate elif elsif
		" typo of ->
		imap -< ->
	elseif &ft == 'php'
		abbreviate elsif elseif
		abbreviate elif elseif
		" typo of ->
		imap -< ->
		" Used to perl, so switch some common perl things into their
		" PHP version
		abbreviate # //
		abbreviate eq ==
		abbreviate sub function
	elseif &ft == 'c' || &ft == 'cpp' || &ft == 'javascript'
		abbreviate elsif else if
		abbreviate elif else if
		abbreviate elseif else if
		abbreviate eq ==
	endif
endfun
autocmd BufRead,BufNewFile * :call PerFT_typo()

" Copyright updates
"
" The one with the extra spaces is actually useful for headers where the
" spacing is used for formatting purposes.
silent! call copyright#UpdateFor('Eskild Hustvedt','  Eskild Hustvedt');
"}}}

" Local RC file (main/tail-version), can be used for instance to fix backspace on old boxes {{{
if filereadable(glob("~/.vimrc_local"))
	source ~/.vimrc_local
endif "}}}
" vim: set foldmethod=marker :
